﻿using gitlab_ci_view.Properties;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace gitlab_ci_view
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            tb_url.Text = Settings.Default.url;
            tb_project.Text = Settings.Default.project;
            // not running mono?
            if (Type.GetType("Mono.Runtime") == null)
            {
                this.MinimumSize = new System.Drawing.Size(710, 470);
                this.ClientSize = new System.Drawing.Size(694, 431);
                this.progressBar1.Location = new System.Drawing.Point(12, 396);
                this.progressBar1.Size = new System.Drawing.Size(575, 23);
                this.dataGridView1.Size = new System.Drawing.Size(670, 301);
                this.button1.Location = new System.Drawing.Point(607, 396);
                this.tb_project.Size = new System.Drawing.Size(179, 20);
                this.lbl_projectID.Location = new System.Drawing.Point(605, 35);
                this.lbl_sum.Location = new System.Drawing.Point(567, 67);
            }
        }

        private bool catchPage(int page, DataTable datatab, ref int maxpage, ref double sumartifacts)
        {
            string url = String.Format(
                "{0}/projects/{1}/pipelines/{2}/jobs?per_page=25&page={3}&include_retried=true",
                tb_url.Text, lbl_projectID.Text, comboBox1.SelectedValue, page
            );
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Accept = "application/json";
            request.Headers.Add("PRIVATE-TOKEN", tb_privApiToken.Text);
            WebResponse response;
            try
            {
                response = request.GetResponse();
            } catch (System.Net.WebException e)
            {
                MessageBox.Show(e.Message, "Oh!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                string json = sr.ReadToEnd();
                //System.Console.WriteLine(json);
                JArray a = JArray.Parse(json);

                foreach (JObject data in a)
                {
                    DataRow R = datatab.NewRow();
                    R["id"] = data["id"];
                    R["name"] = data["name"];
                    R["stage"] = data["stage"];
                    R["status"] = data["status"];
                    if (data["status"].ToString().Equals("failed"))
                    {
                        R[4] = data["failure_reason"];
                    }
                    R["started"] = data["started_at"];
                    R["finished"] = data["finished_at"];
                    R["link"] = data["web_url"];
                    if (data["artifacts"].Count() > 1)
                    {
                        R["bytes"] = data["artifacts_file"]["size"];
                        sumartifacts += int.Parse(data["artifacts_file"]["size"].ToString());
                    }
                    if (
                        !data["status"].ToString().Equals("pending") &&
                        !data["status"].ToString().Equals("skipped") &&
                        !data["status"].ToString().Equals("manual") &&
                        !data["status"].ToString().Equals("canceled") &&
                        !data["status"].ToString().Equals("created")
                        )
                    {
                        R["runner"] = data["runner"]["description"];
                    }
                    datatab.Rows.Add(R);
                }
                maxpage = int.Parse(response.Headers["x-total-pages"]);
            }
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool success = false;
            double sumartifacts = 0;
            int page = 1;
            int maxpage = 1;
            progressBar1.Value = 0;

            DataTable datatab = new DataTable();
            datatab.Columns.Add("id");
            datatab.Columns.Add("name");
            datatab.Columns.Add("stage");
            datatab.Columns.Add("status");
            datatab.Columns.Add("failure");
            datatab.Columns.Add("started");
            datatab.Columns.Add("finished");
            datatab.Columns.Add("link");
            datatab.Columns.Add("bytes");
            datatab.Columns.Add("runner");

            success = catchPage(page, datatab, ref maxpage, ref sumartifacts);
            if (!success) return;
            progressBar1.Value = (int)(100 * ((float)page / (float)maxpage));
            if (maxpage > 1)
            {
                for(page=2; page<=maxpage; page++)
                {
                    success = catchPage(page, datatab, ref maxpage, ref sumartifacts);
                    if (!success) return;
                    progressBar1.Value = (int)(100 * ((float)page / (float)maxpage));
                }
            }
            lbl_sum.Text = String.Format("{0:N} kB", sumartifacts/1024.0);
            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
            dataGridView1.DataSource = datatab;
            dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            Settings.Default.url = tb_url.Text;
            Settings.Default.project = tb_project.Text;
            Settings.Default.Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string url = String.Format(
                "{0}/projects/{1}",
                tb_url.Text, HttpUtility.UrlEncode(tb_project.Text)
            );
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Accept = "application/json";
            request.Headers.Add("PRIVATE-TOKEN", tb_privApiToken.Text);
            WebResponse response;
            try
            {
                response = request.GetResponse();
            }
            catch (System.Net.WebException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                string json = sr.ReadToEnd();
                JObject o = JObject.Parse(json);
                lbl_projectID.Text = o["id"].ToString();
            }

            url = String.Format(
                "{0}/projects/{1}/pipelines?per_page=50&page=1",
                tb_url.Text, lbl_projectID.Text
            );
            HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create(url);
            request2.Method = "GET";
            request2.Accept = "application/json";
            request2.Headers.Add("PRIVATE-TOKEN", tb_privApiToken.Text);
            try
            {
                response = request2.GetResponse();
            }
            catch (System.Net.WebException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var dataSource = new List<Pipeline>();
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                string json = sr.ReadToEnd();
                JArray a = JArray.Parse(json);
                foreach (JObject data in a)
                {
                    string val = String.Format("{0}: {1}, {2}", data["id"], data["ref"], data["status"]);
                    dataSource.Add(new Pipeline() {id = data["id"].ToString(), Value = val });
                }
                comboBox1.DataSource = dataSource;
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "Value";
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            Settings.Default.url = tb_url.Text;
            Settings.Default.project = tb_project.Text;
            Settings.Default.Save();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                dataGridView1.SelectionMode = DataGridViewSelectionMode.CellSelect;
                if (e.ColumnIndex == 7) // the url
                {
                    DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                    string link = row.Cells[e.ColumnIndex].Value.ToString();
                    System.Diagnostics.Process.Start(link);
                }
            }
        }
    }
}
