﻿# Gitlab-Ci-View

![Screenshot on Linux](img/screenshot2.png)

It is a hacky single thread cSharp GUI to fetch the last 50 Pipelines
of gitlab (Personal Access Tokens, api needed), select one pipeline
(Dropdown list with id,branch,status) and fetch these job infos:

- id, name, status, stage
- on failure: see the reason
- start and end time
- link to the job-log
- runner description (depends on status)

It is good for beginners and how to handle Datagrid, Newtonsoft.Json and the
REST-API of gitlab (or other REST/Json stuff).

![Screenshot on Win10](img/screenshot.png)

## Nice

- double click on row to open default browser and see the job log
- the sum of all artifact zip files
- easy copy-paste to excel
- stores app settings "api url" and "project"
- not overloaded gui, small fooprint
- tested on pipelines with more than 300 jobs and in many different states

## Download

Look/Browse at my pipeline artifacts, e.g. [Download this Mono Build](https://gitlab.com/deadlockz/gitlab-ci-view/-/jobs/6942414317/artifacts/browse).

## Issues

- not store the api-key... I miss a secure concept for that
- maybe ssl/https handling is missing
- blocking single thread stuff
- less/bad error handling
- missing bulk delete all artifacts
- missing buttons in each row to cancel, start, retry a job
- missing deactive buttons

If you have time and want to learn and make stable and greate GUIs, then implement it!
I do not want to blow up this code, because it should just be more comfortable then
a bash script with `curl` and `json_pp`.

## Mono and cSharp notes on debian12

Maybe on Windows https://www.mono-project.com/ mono is a good choice, too.

- `apt install dirmngr ca-certificates gnupg`
- `gpg --homedir /tmp --no-default-keyring --keyring /usr/share/keyrings/mono-official-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF`
- `apt update`
- `echo "deb [signed-by=/usr/share/keyrings/mono-official-archive-keyring.gpg] https://download.mono-project.com/repo/debian stable-buster main" | tee /etc/apt/sources.list.d/mono-official-stable.list`
- `apt update`
- `apt install mono-complete`

## Build and dependencies

Download nupkg from https://www.nuget.org/packages/Newtonsoft.Json/13.0.3 and use e.g. 7zip to extract lib/net45/Newtonsoft.Json.dll (yes, on Linux too!).

![7zip to look inside nupkg](img/nupkg.png)

I describe the cli terminal/cmd way here! I do not describe a different build way
like msbuild, cmake, MonoDevelop, SharpDevelop, Microsoft-Code or VisualStudio 2022 Community.

### .NET 4 on windows (cmd)

Part of .NET framework should be the free **csc** Compiler:

- `c:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe /t:exe /out:gitlab-ci-view.exe src\Form1.cs src\Form1.Designer.cs src\Program.cs src\Settings.cs src\Settings.Designer.cs /r:System.Windows.Forms.dll /r:System.Drawing.dll /r:Newtonsoft.Json.dll /r:System.Data.dll /r:System.Xml.dll /r:System.Net.Http.dll /r:System.Deployment.dll /r:System.Data.DataSetExtensions.dll /r:System.Xml.Linq.dll /r:System.Web.dll`
- run it with `.\gitlab-ci-view.exe`

### Mono on Linux

Part of Mono should be the opensource **mcs** Compiler:

- `mcs -out:gitlab-ci-view.exe src/Form1.cs src/Form1.Designer.cs src/Program.cs src/Settings.cs src/Settings.Designer.cs /r:System.Windows.Forms.dll /r:System.Drawing.dll /r:Newtonsoft.Json.dll /r:System.Data.dll /r:System.Xml.dll /r:System.Net.Http.dll /r:System.Deployment.dll /r:System.Data.DataSetExtensions.dll /r:System.Xml.Linq.dll /r:System.Web.dll`
- run it with `mono gitlab-ci-view.exe`


## (un)License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to http://unlicense.org

### I link to Newtonsoft.Json

Newtonsoft.Json is not under Unlicense! See details from [newtonsoft.com](https://www.newtonsoft.com/json).
